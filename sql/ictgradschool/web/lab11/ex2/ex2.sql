-- Answers to exercise 2 questions
SELECT s.fname, s.lname
FROM unidb_students as s, unidb_attend as a
WHERE s.id = a.id
AND a.dept = "comp"
AND a.num = 219;

SELECT s.fname, s.lname
FROM unidb_students as s, unidb_courses as c
WHERE s.id = c.rep_id
AND s.country <> "NZ";

SELECT l.office
FROM unidb_lecturers as l, unidb_teach as t
WHERE l.staff_no = t.staff_no
AND t.dept = "comp"
AND t.num = 219;

SELECT s.fname, s.lname
FROM unidb_students as s, unidb_attend as a, unidb_teach as t, unidb_lecturers as l
WHERE s.id = a.id
AND a.dept = t.dept
AND a.num = t.num
AND t.staff_no = l.staff_no
AND l.fname = "Te Taka";

SELECT id
FROM unidb_students
UNION
  SELECT mentor
FROM unidb_students;

SELECT fname, lname
FROM unidb_lecturers
WHERE office LIKE "G%"
UNION
  SELECT fname, lname
FROM unidb_students
WHERE country <> "NZ";

SELECT coord_no
FROM unidb_courses
WHERE dept = "comp"
AND num = 219
UNION
  SELECT rep_id
FROM unidb_courses
WHERE dept = "comp"
AND num = 219;


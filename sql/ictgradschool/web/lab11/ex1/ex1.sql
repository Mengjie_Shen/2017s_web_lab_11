-- Answers to exercise 1 questions
SELECT DISTINCT dept
FROM unidb_courses;

SELECT DISTINCT semester
FROM unidb_attend;

SELECT DISTINCT dept, num
FROM unidb_attend;

SELECT fname, lname, country
FROM unidb_students
ORDER BY fname;

SELECT fname, lname, mentor
FROM unidb_students
ORDER BY mentor;

SELECT *
FROM unidb_lecturers
ORDER BY office;

SELECT *
FROM unidb_lecturers
WHERE staff_no > 500;

SELECT *
FROM unidb_students
WHERE id BETWEEN 1669 and 1823;

SELECT *
FROM unidb_students
WHERE country IN ("NZ", "AU", "US");

SELECT *
FROM unidb_lecturers
WHERE office LIKE "G%";

SELECT *
FROM unidb_courses
WHERE dept <> "comp";

SELECT *
FROM unidb_students
WHERE country IN ("FR", "MX");
